# Generated by Django 4.0.3 on 2022-04-14 19:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0002_location_picture_url'),
    ]

    operations = [
        migrations.AlterField(
            model_name='location',
            name='picture_url',
            field=models.URLField(null=True),
        ),
    ]
