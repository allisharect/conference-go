from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests
from .models import Location

def find_pictures(search_term):
    pexels_url = "https://api.pexels.com/v1/search?query=" + search_term

    headers = {"Authorization": PEXELS_API_KEY}
    r = requests.get(pexels_url, headers=headers)
    r = json.loads(r.content)
    photos = r["photos"]
    if photos and len(photos) > 0:
        return photos


# def get_photo_URL():
#     response = requests.get("https://api.pexels.com/v1/search", headers={"Authorization": PEXELS_API_KEY})
#     photo = json.loads(response.url)
#     photo_url = {
#         "url": photo["url"],
#     }
#     return photo_url


# def get_temp():
#     translate_location = "http://api.openweathermap.org/geo/1.0/direct?q={city name},{state code},{country code}&limit={limit}"
#     headers = {"Authorization": OPEN_WEATHER_API_KEY}
#     response = requests.get(translate_location, headers=headers)
#     response = json.loads(response.content)
#     temp = response["temp"]
#     return temp

def get_weather_data():
    url = "http://api.openweathermap.org/geo/1.0/direct?q=" + city